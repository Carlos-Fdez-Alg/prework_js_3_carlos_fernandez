var letras = ['T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D', 'X', 'B', 'N', 'J', 'Z', 'S', 'Q', 'V', 'H', 'L', 'C', 'K', 'E', 'T'];

function comprobarDNI() {
    let dni = prompt('Introduzca su DNI sin letra');
    let letra = prompt('Introduzca la letra de su DNI');
    if (dni < 0 || dni > 99999999) {
        console.log('El número de DNI proporcionado no es válido');    
    } else {
        let resto = dni % 23;
        if (letra === letras[resto]) {
            console.log('DNI válido');        
        } else {
            console.log('DNI incorrecto');        
        }
    }
}

comprobarDNI();

//Me da un error y me dice que window no está definido cuando lo ejecuto en la terminal. Pero si me funciona cuando lo incrusto en un html. ¿Cómo lo soluciono?